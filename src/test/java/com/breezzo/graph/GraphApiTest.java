package com.breezzo.graph;


import com.breezzo.graph.search.Bfs;
import com.breezzo.graph.search.PathFinder;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;

public class GraphApiTest {

    final PathFinder<Integer> pathFinder = new Bfs<>();

    @Test
    void pathDoesNotExists_whenGraphIsEmpty() {
        assertThat(directionalGraph().getPath(0, 1), empty());
    }

    @Test
    void pathDoesNotExists_whenNoPathBetweenVertices() {
        Graph<Integer> g = directionalGraph();
        g.addVertex(1);
        g.addVertex(2);
        assertThat(g.getPath(1, 2), empty());
    }

    @Test
    void pathExists_afterAddEdge() {
        Graph<Integer> g = directionalGraph();
        g.addEdge(1, 2);
        assertThat(g.getPath(1, 2), contains(new SimpleEdge<>(1, 2)));
    }

    @Test
    void pathExists_afterAddVerticesAndEdge() {
        Graph<Integer> g = directionalGraph();
        g.addVertex(1);
        g.addVertex(2);
        g.addVertex(3);
        g.addEdge(1, 3);
        assertThat(g.getPath(1, 3), contains(new SimpleEdge<>(1, 3)));
    }

    @Test
    void pathExists_withSeveralEdges() {
        Graph<Integer> g = directionalGraph();
        g.addEdge(3, 5);
        g.addEdge(1, 2);
        g.addEdge(2, 3);
        g.addVertex(4);
        g.addEdge(5, 6);

        assertThat(
                g.getPath(1, 6),
                contains(
                        new SimpleEdge<>(1, 2),
                        new SimpleEdge<>(2, 3),
                        new SimpleEdge<>(3, 5),
                        new SimpleEdge<>(5, 6)
                )
        );
    }

    @Test
    void pathExistsBothDirections_inBidirectionalGraph() {
        Graph<Integer> g = new BiDirectionalGraph<>(pathFinder);
        g.addEdge(1, 2);
        g.addEdge(2, 3);
        g.addEdge(3, 4);

        assertThat(g.getPath(1, 4), contains(new SimpleEdge<>(1, 2), new SimpleEdge<>(2, 3), new SimpleEdge<>(3, 4)));
        assertThat(g.getPath(4, 1), contains(new SimpleEdge<>(4, 3), new SimpleEdge<>(3, 2), new SimpleEdge<>(2, 1)));
    }

    private DirectionalGraph<Integer> directionalGraph() {
        return new DirectionalGraph<>(pathFinder);
    }
}
