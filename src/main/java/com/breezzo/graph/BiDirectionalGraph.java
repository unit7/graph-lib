package com.breezzo.graph;

import com.breezzo.graph.search.PathFinder;

public class BiDirectionalGraph<V> extends DirectionalGraph<V> {

    public BiDirectionalGraph(PathFinder<V> pathFinder) {
        super(pathFinder);
    }

    @Override
    public void addEdge(V from, V to) {
        super.addEdge(from, to);
        super.addEdge(to, from);
    }
}
