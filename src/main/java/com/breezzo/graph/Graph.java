package com.breezzo.graph;

import java.util.List;

public interface Graph<V> extends GraphOperations<V> {

    /**
     * Adds vertex to the graph if it is not present yet.
     */
    void addVertex(V vertex);

    /**
     * Adds edge between two vertices. If any of two vertices is not present in the graph then it will be added.
     */
    void addEdge(V from, V to);

    /**
     * Returns path between two vertices. It is not guaranteed that the return path is the shortest.
     * @return list of edges between two vertices in lexicographical order if any path exists or empty list if it is not.
     */
    List<Edge<V>> getPath(V from, V to);
}
