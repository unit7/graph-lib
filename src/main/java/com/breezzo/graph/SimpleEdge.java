package com.breezzo.graph;

import java.util.Objects;

public class SimpleEdge<Vertex> implements Edge<Vertex> {
    private final Vertex from;
    private final Vertex to;

    public SimpleEdge(Vertex from, Vertex to) {
        this.from = from;
        this.to = to;
    }

    @Override
    public Vertex getFrom() {
        return from;
    }

    @Override
    public Vertex getTo() {
        return to;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        SimpleEdge<?> that = (SimpleEdge<?>) o;
        return from.equals(that.from) &&
                to.equals(that.to);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to);
    }

    @Override
    public String toString() {
        return from + "->" + to;
    }
}
