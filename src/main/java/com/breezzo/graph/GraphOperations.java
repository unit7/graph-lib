package com.breezzo.graph;

import java.util.Collection;

/**
 * Base abstraction that defines a set of graph operations.
 * @param <V> vertex type
 */
public interface GraphOperations<V> {

    /**
     * Returns outgoing edges from specified vertex.
     * @param vertex vertex from which edges should be obtained
     * @return collection of outgoing edges or empty if none
     */
    Collection<Edge<V>> getEdgesOut(V vertex);
}
