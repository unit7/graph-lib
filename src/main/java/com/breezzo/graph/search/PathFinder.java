package com.breezzo.graph.search;

import com.breezzo.graph.Edge;
import com.breezzo.graph.GraphOperations;

import java.util.List;

public interface PathFinder<Vertex> {

    List<Edge<Vertex>> find(Vertex from, Vertex to, GraphOperations<Vertex> graphOperations);
}
