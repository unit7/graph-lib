package com.breezzo.graph.search;

import com.breezzo.graph.Edge;
import com.breezzo.graph.GraphOperations;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Set;

/**
 * Breadth-first search algorithm.
 */
public class Bfs<Vertex> implements PathFinder<Vertex> {

    @Override
    public List<Edge<Vertex>> find(Vertex from, Vertex to, GraphOperations<Vertex> graphOperations) {
        Context<Vertex> ctx = Context.of(from, to);
        while (ctx.hasUnvisitedVertices()) {
            Vertex nextVertex = ctx.next();
            Collection<Edge<Vertex>> edges = graphOperations.getEdgesOut(nextVertex);
            traverse(ctx, edges);
            if (ctx.isPathFound()) {
                return ctx.resultPath;
            }
        }
        return ctx.resultPath;
    }

    private void traverse(Context<Vertex> ctx, Collection<Edge<Vertex>> edges) {
        for (Edge<Vertex> edge : edges) {
            Vertex next = edge.getTo();
            if (ctx.isVisited(next)) {
                continue;
            }
            ctx.add(edge);
            if (next.equals(ctx.to)) {
                ctx.buildResultPath();
                break;
            }
        }
    }

    private static class Context<Vertex> {
        Vertex to;
        Queue<Vertex> vertexQueue = new LinkedList<>();
        Set<Vertex> seenVertices = new HashSet<>();
        Map<Vertex, Edge<Vertex>> vertexTrace = new HashMap<>();
        List<Edge<Vertex>> resultPath = new ArrayList<>();

        static <V> Context<V> of(V from, V to) {
            Context<V> context = new Context<>();
            context.vertexQueue.add(from);
            context.to = to;
            return context;
        }

        boolean hasUnvisitedVertices() {
            return !vertexQueue.isEmpty();
        }

        Vertex next() {
            Vertex v = vertexQueue.poll();
            seenVertices.add(v);
            return v;
        }

        boolean isVisited(Vertex v) {
            return seenVertices.contains(v);
        }

        void add(Edge<Vertex> edge) {
            Vertex to = edge.getTo();
            vertexQueue.add(to);
            vertexTrace.put(to, edge);
        }

        void buildResultPath() {
            Edge<Vertex> previousEdge = vertexTrace.get(to);
            do {
                resultPath.add(previousEdge);
                previousEdge = vertexTrace.get(previousEdge.getFrom());
            } while (previousEdge != null);
            Collections.reverse(resultPath);
        }

        boolean isPathFound() {
            return !resultPath.isEmpty();
        }
    }
}
