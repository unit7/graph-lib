package com.breezzo.graph;

import com.breezzo.graph.search.PathFinder;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DirectionalGraph<V> implements Graph<V> {
    private final Map<V, Set<Edge<V>>> adjacencyMap = new HashMap<>();
    private final PathFinder<V> pathFinder;

    public DirectionalGraph(PathFinder<V> pathFinder) {
        this.pathFinder = pathFinder;
    }

    @Override
    public void addVertex(V vertex) {
        addVertexIfMissing(vertex);
    }

    private Set<Edge<V>> addVertexIfMissing(V vertex) {
        return adjacencyMap.computeIfAbsent(vertex, key -> new HashSet<>());
    }

    @Override
    public void addEdge(V from, V to) {
        Set<Edge<V>> edges = addVertexIfMissing(from);
        edges.add(new SimpleEdge<>(from, to));
        addVertexIfMissing(to);
    }

    @Override
    public List<Edge<V>> getPath(V from, V to) {
        return pathFinder.find(from, to, this);
    }

    @Override
    public Collection<Edge<V>> getEdgesOut(V vertex) {
        return adjacencyMap.getOrDefault(vertex, Collections.emptySet());
    }
}
