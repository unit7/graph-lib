package com.breezzo.graph;

public interface Edge<Vertex> {

    Vertex getFrom();

    Vertex getTo();
}
